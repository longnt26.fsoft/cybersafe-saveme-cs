﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CyberSafe_SaveMe.Models
{
    public class PasswordChange
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        [DataType(DataType.Password)]
        public string newPass { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        [Compare(nameof(newPass), ErrorMessage = "Confirm password doesn't match!!!")]
        [DataType(DataType.Password)]
        public string confirmPass { get; set; }
    }
}