﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CyberSafe_SaveMe.Models;

namespace CyberSafe_SaveMe.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            DataTable tbProduct = new DataTable();
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Products", connection);
                sqlDa.Fill(tbProduct);
            }
            return View(tbProduct);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            Product product = new Product();
            DataTable tbProduct = new DataTable();
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                string query = "select * from Products where ProductID = " + id + "";
                SqlDataAdapter sqlDa = new SqlDataAdapter(query, connection);
                sqlDa.Fill(tbProduct);
            }
            if (tbProduct.Rows.Count == 1)
            {
                product.ProductID = Convert.ToInt32(tbProduct.Rows[0][0].ToString());
                product.ProductName = tbProduct.Rows[0][1].ToString();
                product.Price = Convert.ToDecimal(tbProduct.Rows[0][2].ToString());
                product.Quantity = Convert.ToInt32(tbProduct.Rows[0][3].ToString());
                product.Description = tbProduct.Rows[0][4].ToString();
                return View(product);
            }
            else
            {
                return RedirectToAction("Index");
            }          
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View(new Product());
        }

        // POST: Product/Create
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(Product product)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                string query = "Insert into Products(Name, Price, Quantity, Description) values ('" + product.ProductName + "', " + product.Price + ", " + product.Quantity + ", '" + product.Description + "')";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // GET: Product/Edit/5
            public ActionResult Edit(int id)
            {
                Product product = new Product();
                DataTable tbProduct = new DataTable();
                string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (var connection = new SqlConnection(cs))
                {
                    connection.Open();
                    string query = "select * from Products where ProductID = " + id + "";
                    SqlDataAdapter sqlDa = new SqlDataAdapter(query, connection);
                    sqlDa.Fill(tbProduct);
                }
                if (tbProduct.Rows.Count == 1)
                {
                    product.ProductID = Convert.ToInt32(tbProduct.Rows[0][0].ToString());
                    product.ProductName = tbProduct.Rows[0][1].ToString();
                    product.Price = Convert.ToDecimal(tbProduct.Rows[0][2].ToString());
                    product.Quantity = Convert.ToInt32(tbProduct.Rows[0][3].ToString());
                    product.Description = tbProduct.Rows[0][4].ToString();
                    return View(product);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }

            // POST: Product/Edit/5
            [HttpPost]
            public ActionResult Edit(Product product)
            {
                string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (var connection = new SqlConnection(cs))
                {
                    connection.Open();
                    string query = "Update Products set Name ='" + product.ProductName + "', Price =" + product.Price + ", Quantity = " + product.Quantity + ", Description='" + product.Description + "' where ProductID =" + product.ProductID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                return RedirectToAction("Index");
            }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                string query = "Delete from Products where ProductID = " + id + "";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }
    }
}
