﻿using CyberSafe_SaveMe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CyberSafe_SaveMe.Controllers
{
    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult NavigationBar()
        {
            var model = new User();
            return PartialView(model);
        }
    }
}