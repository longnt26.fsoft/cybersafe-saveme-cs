﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CyberSafe_SaveMe.Common;
using CyberSafe_SaveMe.Models;

namespace CyberSafe_SaveMe.Controllers
{
    public class LoginController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Validate(User user)
        {
            try
            {
                User curUser = new User();
                DataTable tbUser = new DataTable();
                string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (var connection = new SqlConnection(cs))
                {
                    connection.Open();
                    string commandText = "select * from Users where Username = '" + user.Username + "' and Passwd = '" + user.Password + "'";
                    SqlDataAdapter sqlDa = new SqlDataAdapter(commandText, connection);
                    sqlDa.Fill(tbUser);                  
                }
                if (tbUser.Rows.Count != 0)
                {
                    var userSession = new UserLogin();
                    userSession.UserID = Convert.ToInt32(tbUser.Rows[0][0].ToString());
                    userSession.Username = user.Username;
                    Session.Add(CommonConstants.USER_SESSION, userSession);
                    return RedirectToAction("Index", "Home");
                }
                TempData["Message"] = "Login failed. Username or password supplied doesn't exist.";             
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Login failed. Error - " + ex.Message;
            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            return Redirect("/Login/Index");
        }

        [HttpGet]
        public ActionResult LoadUser()
        {
            return View();
        }

    }
}