﻿using CyberSafe_SaveMe.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CyberSafe_SaveMe.Controllers
{
    public class UserController : Controller
    {
        // GET: User     
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //Create new User 
        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (var connection = new SqlConnection(cs))
                {
                    connection.Open();
                    string query = "Insert into Users(Username, Passwd, Name, Phone, Address) values ('" + user.Username + "', '" + user.Password + "', '" + user.Name + "', " + user.Phone + ", '" + user.Address + "')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                if (user.UserID > 0)
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    ModelState.AddModelError("", "Add User Successfully!");
                }
            }
            return View("Index");
        }

        //View Detail Information/Profile of User.
        public ActionResult Details(int id)
        {
            User user = new User();
            DataTable tbUser = new DataTable();
            String cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                string query = "select * from Users where UserID = " + id + "";
                SqlDataAdapter sqlDA = new SqlDataAdapter(query, connection);
                sqlDA.Fill(tbUser);
            }
            if (tbUser.Rows.Count == 1)
            {
                user.UserID = Convert.ToInt32(tbUser.Rows[0][0].ToString());
                user.Username = tbUser.Rows[0][1].ToString();
                user.Password = tbUser.Rows[0][2].ToString();
                user.Name = tbUser.Rows[0][3].ToString();
                user.Phone = tbUser.Rows[0][4].ToString();
                user.Address = tbUser.Rows[0][5].ToString();
                return View(user);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        public ActionResult ChangePassword(int id)
        {
            PasswordChange changePass = new PasswordChange();
            changePass.UserID = id;
            return View(changePass);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult ChangePassword(PasswordChange passChange)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(cs))
                
            {
                connection.Open();
                string query = "update Users set Passwd = '" + passChange.newPass + "' where UserID = " + passChange.UserID + "";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }
    }
}